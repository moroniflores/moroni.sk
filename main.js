const controller = new ScrollMagic.Controller();

const wipeAnimation = gsap.timeline()
	.fromTo("section.panel.second", 1, {x: "-100%"}, {x: "0%", ease: Linear.easeNone, delay: 1})  // in from left
	.fromTo("section.panel.third",    1, {x:  "100%"}, {x: "0%", ease: Linear.easeNone, delay: 1})  // in from right
	.fromTo("section.panel.fourth", 1, {y: "-100%"}, {y: "0%", ease: Linear.easeNone, delay: 1}); // in from top

// create scene to pin and link animation
new ScrollMagic.Scene({
		triggerElement: "#pinContainer",
		triggerHook: "onLeave",
		duration: "300%"
	})
	.setPin("#pinContainer")
	.setTween(wipeAnimation)
	.addTo(controller);
