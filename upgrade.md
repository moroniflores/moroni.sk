# Kernel upgrade
Get the target build from https://github.com/raspberrypi/linux/tags

```
# cd /usr/src/
# wget https://github.com/raspberrypi/linux/archive/refs/tags/{tag}.tar.gz
# tar xf raspberrypi-linux-{tag}.tar.gz
```
List the available kernel sources. The asterisk marks the current sources.

```
# eselect kernel list
Available kernel symlink targets:
  [1]   linux-1.20210805
  [2]   linux-1.20210831 *
  [3]   linux-1.20220120
```

Set the kernel sources to the new target 

```
# eselect kernel set 3
```

Move to the new kernel folder

```
# cd /usr/src/linux
```
Copy the previous kernel configuration

If the kernel option _Enable access to .config through /proc/config.gz_ was enabled for the current kernel (CONFIG_IKCONFIG_PROC):

```
# zcat /proc/config.gz > /usr/src/linux/.config
```

Or simply copy it from the currently running kernel:

```
# cp /usr/src/{current-kernel}/.config /usr/src/linux/.config
```

Update the .config file using `make oldconfig` for an interactive configuration of new options, or `make olddefconfig` to set the new options to their default. 

Generate configs for Broadcom BCM2711

```
# make bcm2711_defconfig
```

If needed, use `make menuconfig` to adjust other options (e.g. `CONFIG_LOCALVERSION`).

Once you're ready, build the kernel:

```
# make -j4 Image modules dtbs
```

To install the new kernel make sure that the `/boot` partition is mounted, then copy the drivers and bkernel binary:

```
# make modules_install dtbs_install
# cp /usr/src/linux/arch/arm64/boot/dts/broadcom/bcm2711-rpi-400.dtb /boot
# cp /usr/src/linux/arch/arm64/boot/Image /boot/kernel8.img
```